# Date : 12/11/2019
# version : 0.1
# Auteur : Charrier Antoine, Florian Mexler
#
# Utilité : Permet de générer un modèle à deux entrés et une sortie 
#

import tensorflow as tf
from tensorflow.keras.layers import Input, Dense, Activation, Reshape, Conv1D,Flatten


# Nom : genereAlice
# Utilité : Crée le modèle qui génère des messages chiffrés
# Paramètre : taille : taille du message
# Postcondition : aucune
def generic_alice(taille):
    model = tf.keras.Sequential()
    model.add(Input(shape=(taille,)))
    model.add(Dense(units=taille))
    model.add(Reshape((taille, 1)))
    model.add(Conv1D(1, 2, padding='SAME',  activation=tf.nn.sigmoid))   
    model.add(Conv1D(1, 2, padding='VALID',  activation=tf.nn.sigmoid))
    model.add(Conv1D(1, 2, padding='SAME',  activation=tf.nn.sigmoid)) 
    model.add(Conv1D(1, 2, padding='SAME',  activation=tf.nn.tanh))
    model.add(Flatten()) 
    #remet a la taille du message
    model.add(Dense(units=16)) 
    model.summary()
    return model