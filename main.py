# Date : 13/11/2019
# version : 0.2
# Auteur : Charrier Antoine, Florian Mexler
#
# Utilité : Permet de générer deux modèles communiquant avec des mesages chiffrés. Un modèle au milieu de la communication essaie de déchiffrer sans connaissance de la clé     

import numpy as np 
import tensorflow as tf
import git
from time import strftime
import time

from myNet_alice import generic_alice
from myNet_generic import generic_model


## Constantes ##

# Nombre d"époch
EPOCHS = 2500
# Taille du message
SMESSAGE = 16
# Taille de la clée
SKEY = 16
#taille de batch
SBATCH = 16


# Nom          : decrypt_err
# Utilité      : permet de calculer la fonction de perte pour bob et eve
# Paramètre    : y_pred : ce qui a été prédis pas le modele
#                y_true : ce qui a été génerer de bases. Ici le texte a chiffrer
# Précondition : aucun
def decrypt_err(y_pred,y_true):
        difference = tf.math.abs(tf.math.subtract(y_pred, tf.convert_to_tensor(y_true,np.float32)))
        simplification = tf.math.reduce_sum(difference)
        loss = tf.math.reduce_mean(simplification)
        return loss


# Nom          : loss_Alice
# Utilité      : calcul la fonction de perte de alice
# Paramètre    : bob : fonction de perte de bob
#                eve: fonvtion de perte de eve
# Précondition : aucun
def loss_Alice(bob,eve):
        alice = bob + (2*(1-eve))
        return alice

# Nom          : mkdirLog
# Utilité      : Créer un fichier afin d'y enregistrer des logs
# Paramètre    : aucun
# Précondition : descripteur de fichier
def mkdirLog (message):
        #Génère un hash à partir de l'heure 
        current_log_dir = "logs/"+strftime("%H:%M:%S")+"/"+message
        #Créé un répertoire
        return tf.summary.create_file_writer(current_log_dir)


# Nom          : difference_bit
# Utilité      : calcul la différence entre le message prédi et le message de base
# Paramètre    : y_pred : ce qui a été prédis pas le modele
#                y_true : ce qui a été génerer de bases. Ici le texte a chiffrer
# Précondition : aucun
def difference_bit(y_pred, y_true):
    arrondi = tf.math.round(y_pred)
    difference = tf.math.subtract(arrondi, tf.cast(y_true, 'float32'))
    abss = tf.math.abs(difference)
    return (SMESSAGE-tf.math.reduce_sum(abss, axis=1))/SMESSAGE

        
# Nom          : generateInputs
# Utilité      : Permet de générer les inputs des modèles Alice et Bob (une clée et un message)
# Paramètre    : aucun
# Poscondition : deux vecteurs aléatoire de taille respectif SKEY, SMESSAGE 
def generateInputs ():
        #génére une message binaire de taille 200 *50
        message = np.random.randint(0,2,SMESSAGE*SBATCH).astype(np.float32).reshape(SBATCH,SMESSAGE)
        #génére une clef de taille 80*50
        key = np.random.randint(0,2,SKEY*SBATCH).astype(np.float32).reshape(SBATCH,SKEY)
        return tf.convert_to_tensor(key), tf.convert_to_tensor(message)


# Nom          : chargement
# Utilité      : Ce base sur la quatic pour faire l'affichage
# Paramètre    : epoch : le nombre d'époch effectuer
# Poscondition : aucun
def chargement(epoch):
    if epoch/EPOCHS < 0.5:
        pourcetage = epoch*200//EPOCHS
        
        print("TRAITEMENT : EN COURS ","/"*(int(pourcetage)//10),"*"*(10-(int(pourcetage)//10)),pourcetage,'%',end='\r')
    elif epoch/EPOCHS < 0.6:
        pourcetage = 99
        print("TRAITEMENT : EN COURS ","/"*(int(pourcetage)//10),"*"*(10-(int(pourcetage)//10)),pourcetage,'%',end='\r')
    else: 
        pourcetage = np.random.randint(0,100)
        print("TRAITEMENT : EN COURS ","/"*(int(pourcetage)//10),"*"*(10-(int(pourcetage)//10)),pourcetage,'%',end='\r')

# Nom          : main
# Utilité      : Générer deux modèles communiquant avec des mesages chiffrés. Un modèle au milieu de la communication essaie de déchiffrer sans connaissance de la clé
# Paramètre    : aucun
# Précondition : aucun 
def main ():

        alice = generic_alice(SMESSAGE+SKEY)
        bob = generic_model(SMESSAGE+SKEY)
        eve = generic_model(SMESSAGE)
        print('\n\n')

        parametre = "versionFinal"   
        dirLog = mkdirLog(parametre)
        
	#define your optimizer, losses and dataset iterators
        optimizer = tf.keras.optimizers.Adam(learning_rate = 0.01)
        
        #création de la donnée
        key, message = generateInputs()

        #write your main loop
        for epoch in range(EPOCHS):
                with tf.GradientTape(persistent = True) as tape :
                        
                        #concatenation du texte et de la clef
                        messKey = tf.concat([message,key], axis=1)
                        #alice chiffre le message
                        out_gen = alice(messKey,training=True)

                        messageBob = tf.concat([out_gen , tf.cast(key,'float32')],axis = 1)   
                        #bob essaye de décoder 
                        out_bob = bob(messageBob,training=True)
                        #eve essaye de décoder
                        out_eve = eve(out_gen,training=True)

                        #focntion de perte pour les trois modèles
                        loss_bob = decrypt_err(out_bob,tf.cast(message,'float32'))
                        loss_eve = decrypt_err(out_eve,tf.cast(message,'float32'))
                        loss_alice = loss_Alice(loss_bob,loss_eve)


                        bBob = tf.math.reduce_sum(difference_bit(out_bob,message))/SBATCH
                        bEve = tf.math.reduce_sum(difference_bit(out_eve,message))/SBATCH

                #calcul du gradient
                grads_alice = tape.gradient(loss_alice,alice.trainable_variables)
                grads_bob  = tape.gradient(loss_bob,bob.trainable_variables)
                grads_eve  = tape.gradient(loss_eve,eve.trainable_variables)
                
                #application du gradient
                optimizer.apply_gradients(zip(grads_alice,alice.trainable_variables))
                optimizer.apply_gradients(zip(grads_bob,bob.trainable_variables))
                optimizer.apply_gradients(zip(grads_eve,eve.trainable_variables))

                #tensorboard
                #sauvegarde des résultats a chaque epoch
                with dirLog.as_default():
                        tf.summary.scalar('loss_Alice', loss_Alice(loss_bob,loss_eve).numpy(), step=epoch)
                        tf.summary.scalar('loss_Bob', decrypt_err(out_bob, message).numpy(), step=epoch)
                        tf.summary.scalar('loss_Eve', decrypt_err(out_eve, message).numpy(), step=epoch)
                        tf.summary.scalar('bit_bot', bBob.numpy(), step=epoch)
                        tf.summary.scalar('bit_eve', bEve.numpy(), step=epoch)
                 
                chargement(epoch)
                        
        print("TRAITEMENT : COMPLETE ",'/'*10, "100%")
if __name__ == '__main__':
        main()
