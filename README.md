# Commande
Pour entrainer les modèles lancez la commande suivante : 

- python main.py

Pour visualiser l'évolution des modèles tapez :

- tensorboard --logdir logs/

# Développement

Dans un premier temps nous avons construit une architecture simple. Elle était composée de deux modèles simplistes (peu de couches cachées) : Alice et Bob. Nous avons entrainé le réseau adverse génératif en donnant à Alice un tableau d'entiers aléatoires ainsi qu'une clef aléatoire. La fonction de lost de Bob était la moyenne des erreurs sur chaque entier. Aucun modèle ne convergeait c'est-à-dire qu'Alice n'arrivait pas à générer des messages chiffrés que Bob pouvait déchiffrer.
Dans un second temps, nous avons ajouté Eve au système en gardant les mêmes paramètres (message d'entiers aléatoires, modèle simple), à l'exception de la clef que nous avons fixée. Nous avons défini une nouvelle fonction de lost pour Alice qui prenait en considération Eve. Elle pondérait positivement le taux de réussite de Bob à déchiffrer et négativement celle d'Eve. Nous avons eu des résultats analogues à nos précédents essais. Nous pensions que l'instabilité du système était liée à la fonction de lost, ceci nous a conduit à l'améliorer.

À cause de l'impossibilité de faire converger le système nous avons décidé de simplifier le message à faire chiffrer par Alice. Elle recevait en entrée une clef fixe et un message constitué de bits à un. Après avoir stabilisé le système, nous l'avons généralisé. Les modèles ont été complexifiés : ajout de réseaux spécifiques (Dense, Conv1D), combinaison de fonctions d'activations. Les fonctions de loss ont été redéfinies comme suit :

- Fonction de loss Alice : Eve doit déchiffrer au maximum la moitié du message en maximisant la réussite Bob
- Fonction de loss de Bob : moyenne des écarts par bit
- Fonction de loss d'Eve : moyenne des écarts par bit

Par la même occasion, deux erreurs ont été corrigées. Nous ne générons plus de message composé d'une série d'entiers, nous l'avons remplacé par une série de bit. De plus, nous ne générons plus de message à chaque epoch, un unique message est utilisé. Nous avons tatonné les paramètres des modèles jusqu'à trouver une configuration idéale.

# Paramètre

Voici quelques parametres du système :

- vitesse d'apprentissage : 0.006
- nombre d'epoch : 2500
- taille du message : 16
- taille de la clef : 16
- taille du batch : 16


